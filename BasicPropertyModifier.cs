/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.PropertyModifiers
{
    /// <summary>
    /// A generic base class for a property modifier implementation.
    /// </summary>
    /// <typeparam name="T">
    /// The type used as the property value.
    /// </typeparam>
    public class BasicPropertyModifier<T> : IPropertyModifier<T>
    {
        /// <summary>
        /// Holds the value for the <see cref="Value"/> property.
        /// </summary>
        private T value;

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicPropertyModifier{T}"/> class.
        /// </summary>
        /// <param name="value">
        /// The value of the modification.
        /// </param>
        public BasicPropertyModifier(T value)
        {
            this.value = value;
        }                                                                

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicPropertyModifier{T}"/> class.
        /// </summary>
        public BasicPropertyModifier()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets type of modification to perform.
        /// </summary>
        public virtual ModifierType Type { get; set; }

        /// <summary>
        /// Gets or sets value used during modification.
        /// </summary>
        public virtual T Value
        {
            get
            {
                return this.value;
            }
            
            set
            {
                this.value = value;
            }
        }

        public virtual int Priority { get; set; }

        #endregion
    }
}