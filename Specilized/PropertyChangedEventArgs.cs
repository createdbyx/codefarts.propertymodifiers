﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
#if PCL
namespace System.ComponentModel
#else
namespace Codefarts.PropertyModifiers
#endif
{
    using System;

    /// <summary>
    /// Provides data for the <see cref="INotifyPropertyChanged.PropertyChanged" /> event.
    /// </summary>
    public class PropertyChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Holds the name of the property.
        /// </summary>
        private readonly string propertyName;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyChangedEventArgs" /> class.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed. </param>
        public PropertyChangedEventArgs(string propertyName)
        {
            this.propertyName = propertyName;
        }

        /// <summary>Gets the name of the property that changed.</summary>
        /// <returns>The name of the property that changed.</returns>
        public virtual string PropertyName
        {
            get
            {
                return this.propertyName;
            }
        }
    }
}