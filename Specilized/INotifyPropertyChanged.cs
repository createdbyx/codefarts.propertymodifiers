﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

#if PCL
namespace System.ComponentModel
#else
namespace Codefarts.PropertyModifiers
#endif
{
    /// <summary>
    /// Represents the method that will handle the <see cref="INotifyPropertyChanged.PropertyChanged" /> event raised 
    /// when a property is changed on a component.
    /// </summary>
    /// <param name="sender">The source of the event. </param>
    /// <param name="e">A <see cref="PropertyChangedEventArgs" /> that contains the event data. </param>
    public delegate void PropertyChangedEventHandler(object sender, PropertyChangedEventArgs e);

    /// <summary>
    /// Notifies clients that a property value has changed.
    /// </summary>
    public interface INotifyPropertyChanged
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        event PropertyChangedEventHandler PropertyChanged;
    }
}
