/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.PropertyModifiers
{
    using System;
    using System.Collections;

#if UNITY3D
    using Codefarts.Localization;
#else
    using Codefarts.PropertyModifiers.Properties;
#endif

    /// <summary>
    /// Provides a read only implementation for <see cref="IList"/>.
    /// </summary>
    internal class ReadOnlyList : IList
    {
        /// <summary>
        /// Holds a reference to the source list.
        /// </summary>
        private IList sourceList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyList"/> class.
        /// </summary>
        /// <param name="sourceList">
        /// The source list.
        /// </param>
        internal ReadOnlyList(IList sourceList)
        {
            this.sourceList = sourceList;
        }

        /// <summary>
        /// Gets the number of elements contained in the ICollection. (Inherited from ICollection.)
        /// </summary>
        public virtual int Count
        {
            get
            {
                return this.sourceList.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the IList has a fixed size.
        /// </summary>
        public virtual bool IsFixedSize
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the IList is read-only.
        /// </summary>
        public virtual bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether access to the ICollection is synchronized (thread safe). (Inherited from ICollection.)
        /// </summary>
        public virtual bool IsSynchronized
        {
            get
            {
                return this.sourceList.IsSynchronized;
            }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the ICollection. (Inherited from ICollection.)
        /// </summary>
        public virtual object SyncRoot
        {
            get
            {
                return this.sourceList.SyncRoot;
            }
        }

        /// <summary>
        /// Gets or sets the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or set.</param>
        /// <returns>The element at the specified index.</returns>
        public virtual object this[int index]
        {
            get
            {
                return this.sourceList[index];
            }

            set
            {
#if UNITY3D
                var local = LocalizationManager.Instance;
                throw new NotSupportedException(local.Get("NotSupported_ReadOnlyCollection"));
#else
                throw new NotSupportedException(Resources.NotSupported_ReadOnlyCollection);
#endif
            }
        }

        /// <summary>
        /// Adds an item to the IList.
        /// </summary>
        /// <param name="obj">
        /// The object to add to the IList.
        /// </param>
        /// <returns>
        /// The position into which the new element was inserted, or -1 to indicate that the item was not inserted into the collection.
        /// </returns>
        /// <exception cref="NotSupportedException">
        /// Can't add items to a read only list.
        /// </exception>
        public virtual int Add(object obj)
        {
#if UNITY3D
            var local = LocalizationManager.Instance;
            throw new NotSupportedException(local.Get("NotSupported_ReadOnlyCollection"));
#else
            throw new NotSupportedException(Resources.NotSupported_ReadOnlyCollection);
#endif
        }

        /// <summary>
        /// Removes all items from the IList.
        /// </summary>
        public virtual void Clear()
        {
#if UNITY3D
            var local = LocalizationManager.Instance;
            throw new NotSupportedException(local.Get("NotSupported_ReadOnlyCollection"));
#else
            throw new NotSupportedException(Resources.NotSupported_ReadOnlyCollection);
#endif
        }

        /// <summary>
        /// Determines whether the IList contains a specific value.
        /// </summary>
        /// <param name="obj">The object to locate in the IList.</param>
        /// <returns>true if the Object is found in the IList; otherwise, false.</returns>
        public virtual bool Contains(object obj)
        {
            return this.sourceList.Contains(obj);
        }

        /// <summary>
        /// Copies the elements of the ICollection to an Array, starting at a particular Array index. (Inherited from ICollection.)
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the elements copied from ICollection. The Array must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        public virtual void CopyTo(Array array, int index)
        {
            this.sourceList.CopyTo(array, index);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection. (Inherited from IEnumerable.)
        /// </summary>
        /// <returns>An IEnumerator object that can be used to iterate through the collection.</returns>
        public virtual IEnumerator GetEnumerator()
        {
            return this.sourceList.GetEnumerator();
        }

        /// <summary>
        /// Determines the index of a specific item in the IList.
        /// </summary>
        /// <param name="value">The object to locate in the IList.</param>
        /// <returns>The index of value if found in the list; otherwise, -1.</returns>
        public virtual int IndexOf(object value)
        {
            return this.sourceList.IndexOf(value);
        }

        /// <summary>
        /// Inserts an item to the IList at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which value should be inserted.</param>
        /// <param name="obj">The object to insert into the IList.</param>
        public virtual void Insert(int index, object obj)
        {
#if UNITY3D
            var local = LocalizationManager.Instance;
            throw new NotSupportedException(local.Get("NotSupported_ReadOnlyCollection"));
#else
            throw new NotSupportedException(Resources.NotSupported_ReadOnlyCollection);
#endif
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the IList.
        /// </summary>
        /// <param name="value">The object to remove from the IList.</param>
        public virtual void Remove(object value)
        {
#if UNITY3D
            var local = LocalizationManager.Instance;
            throw new NotSupportedException(local.Get("NotSupported_ReadOnlyCollection"));
#else
            throw new NotSupportedException(Resources.NotSupported_ReadOnlyCollection);
#endif
        }

        /// <summary>
        /// Removes the IList item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        public virtual void RemoveAt(int index)
        {
#if UNITY3D
            var local = LocalizationManager.Instance;
            throw new NotSupportedException(local.Get("NotSupported_ReadOnlyCollection"));
#else
            throw new NotSupportedException(Resources.NotSupported_ReadOnlyCollection);
#endif
        }
    }
}