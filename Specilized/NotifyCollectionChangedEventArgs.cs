/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.PropertyModifiers
{
    using System;
    using System.Collections;

#if UNITY3D
    using Codefarts.Localization;
#else
    using Codefarts.PropertyModifiers.Properties;
#endif

    /// <summary>
    /// Provides data for the <see cref="INotifyCollectionChanged.CollectionChanged"/> event.
    /// </summary>
    public class NotifyCollectionChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Holds the action that took place.
        /// </summary>
        private NotifyCollectionChangedAction action;

        /// <summary>
        /// Holds a list of new item that were added.
        /// </summary>
        private IList newItems;

        /// <summary>
        /// Holds the starting index where the new item were added.
        /// </summary>
        private int newStartingIndex;

        /// <summary>
        /// Holds the list of old items that were changed or removed.
        /// </summary>
        private IList oldItems;

        /// <summary>
        /// Holds the old index where the old item were changed or removed.
        /// </summary>
        private int oldStartingIndex;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This must be set to Reset.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If <see cref="action"/> is not set to <see cref="NotifyCollectionChangedAction.Reset"/>.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Reset)
            {
                this.InitializeAdd(action, null, -1);
                return;
            }

#if UNITY3D
            var local = LocalizationManager.Instance;
            throw new ArgumentException(string.Format(local.Get("WrongActionForCtor"), "Reset"), "action");
#else
            throw new ArgumentException(string.Format(Resources.WrongActionForCtor, "Reset"), "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a one-item change.
        /// </summary>
        /// <param name="action">The action that caused the event. This can be set to <see cref="NotifyCollectionChangedAction.Reset"/>, <see cref="NotifyCollectionChangedAction.Add"/>, or <see cref="NotifyCollectionChangedAction.Remove"/>.</param>
        /// <param name="changedItem">The item that is affected by the change.</param>
        /// <exception cref="ArgumentException">
        /// If action is not Reset, Add, or Remove, or if action is Reset and changedItem is not null.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif

            if (action == NotifyCollectionChangedAction.Add || action == NotifyCollectionChangedAction.Remove || action == NotifyCollectionChangedAction.Reset)
            {
                if (action != NotifyCollectionChangedAction.Reset)
                {
                    var objArray = new object[1];
                    objArray[0] = changedItem;
                    this.InitializeAddOrRemove(action, objArray, -1);
                    return;
                }

                if (changedItem == null)
                {
                    this.InitializeAdd(action, null, -1);
                    return;
                }

#if UNITY3D
                local = LocalizationManager.Instance;
                throw new ArgumentException(local.Get("ResetActionRequiresNullItem"), "action");
#else
                throw new ArgumentException(Resources.ResetActionRequiresNullItem, "action");
#endif
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(local.Get("MustBeResetAddOrRemoveActionForCtor"), "action");
#else
            throw new ArgumentException(Resources.MustBeResetAddOrRemoveActionForCtor, "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a one-item change.
        /// </summary>
        /// <param name="action">The action that caused the event. This can be set to <see cref="NotifyCollectionChangedAction.Reset"/>, <see cref="NotifyCollectionChangedAction.Add"/>, or <see cref="NotifyCollectionChangedAction.Remove"/>.</param>
        /// <param name="changedItem">The item that is affected by the change.</param>
        /// <param name="index">The index where the change occurred.</param>
        /// <exception cref="ArgumentException">
        /// If action is not Reset, Add, or Remove, or if action is Reset and changedItem is not null.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem, int index)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif

            if (action == NotifyCollectionChangedAction.Add || action == NotifyCollectionChangedAction.Remove || action == NotifyCollectionChangedAction.Reset)
            {
                if (action != NotifyCollectionChangedAction.Reset)
                {
                    var objArray = new object[1];
                    objArray[0] = changedItem;
                    this.InitializeAddOrRemove(action, objArray, index);
                    return;
                }

                if (changedItem == null)
                {
                    if (index == -1)
                    {
                        this.InitializeAdd(action, null, -1);
                        return;
                    }

#if UNITY3D
                    local = LocalizationManager.Instance;
                    throw new ArgumentException(local.Get("ResetActionRequiresIndexMinus1"), "action");
#else
                    throw new ArgumentException(Resources.ResetActionRequiresIndexMinus1, "action");
#endif
                }

#if UNITY3D
                local = LocalizationManager.Instance;
                throw new ArgumentException(local.Get("ResetActionRequiresNullItem"), "action");
#else
                throw new ArgumentException(Resources.ResetActionRequiresNullItem, "action");
#endif
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(local.Get("MustBeResetAddOrRemoveActionForCtor"), "action");
#else
            throw new ArgumentException(Resources.MustBeResetAddOrRemoveActionForCtor, "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a multi-item change.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This can be set to <see cref="NotifyCollectionChangedAction.Reset"/>, <see cref="NotifyCollectionChangedAction.Add"/>, or <see cref="NotifyCollectionChangedAction.Remove"/>.
        /// </param>
        /// <param name="changedItems">
        /// The items that are affected by the change.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <see cref="changedItems"/> is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// If action is not Reset, Add, or Remove, or if action is Reset and changedItem is not null.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif

            if (action == NotifyCollectionChangedAction.Add || action == NotifyCollectionChangedAction.Remove || action == NotifyCollectionChangedAction.Reset)
            {
                if (action != NotifyCollectionChangedAction.Reset)
                {
                    if (changedItems != null)
                    {
                        this.InitializeAddOrRemove(action, changedItems, -1);
                        return;
                    }

                    throw new ArgumentNullException("changedItems");
                }

                if (changedItems == null)
                {
                    this.InitializeAdd(action, null, -1);
                    return;
                }

#if UNITY3D
                local = LocalizationManager.Instance;
                throw new ArgumentException(local.Get("ResetActionRequiresNullItem"), "action");
#else
                throw new ArgumentException(Resources.ResetActionRequiresNullItem, "action");
#endif
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(local.Get("MustBeResetAddOrRemoveActionForCtor"), "action");
#else
            throw new ArgumentException(Resources.MustBeResetAddOrRemoveActionForCtor, "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a multi-item change or a <see cref="NotifyCollectionChangedAction.Reset"/> change.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This can be set to <see cref="NotifyCollectionChangedAction.Reset"/>, <see cref="NotifyCollectionChangedAction.Add"/>, or <see cref="NotifyCollectionChangedAction.Remove"/>.
        /// </param>
        /// <param name="changedItems">
        /// The items affected by the change.
        /// </param>
        /// <param name="startingIndex">
        /// The index where the change occurred.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If action is not Reset, Add, or Remove, if action is Reset and either changedItems is not null or startingIndex is not -1, or if action is Add or Remove and startingIndex is less than -1.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// If action is Add or Remove and changedItems is null.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems, int startingIndex)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif

            if (action == NotifyCollectionChangedAction.Add || action == NotifyCollectionChangedAction.Remove || action == NotifyCollectionChangedAction.Reset)
            {
                if (action != NotifyCollectionChangedAction.Reset)
                {
                    if (changedItems != null)
                    {
                        if (startingIndex >= -1)
                        {
                            this.InitializeAddOrRemove(action, changedItems, startingIndex);
                            return;
                        }

#if UNITY3D
                        local = LocalizationManager.Instance;
                        throw new ArgumentException(local.Get("IndexCannotBeNegative"), "startingIndex");
#else
                        throw new ArgumentException(Resources.IndexCannotBeNegative, "startingIndex");
#endif
                    }

                    throw new ArgumentNullException("changedItems");
                }

                if (changedItems == null)
                {
                    if (startingIndex == -1)
                    {
                        this.InitializeAdd(action, null, -1);
                        return;
                    }

#if UNITY3D
                    local = LocalizationManager.Instance;
                    throw new ArgumentException(local.Get("ResetActionRequiresIndexMinus1"), "action");
#else
                    throw new ArgumentException(Resources.ResetActionRequiresIndexMinus1, "action");
#endif
                }

#if UNITY3D
                local = LocalizationManager.Instance;
                throw new ArgumentException(local.Get("ResetActionRequiresNullItem"), "action");
#else
                throw new ArgumentException(Resources.ResetActionRequiresNullItem, "action");
#endif
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(local.Get("MustBeResetAddOrRemoveActionForCtor"), "action");
#else
            throw new ArgumentException(Resources.MustBeResetAddOrRemoveActionForCtor, "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a one-item <see cref="NotifyCollectionChangedAction.Replace"/> change.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This can be set to <see cref="NotifyCollectionChangedAction.Replace"/>.
        /// </param>
        /// <param name="newItem">
        /// The new item that is replacing the original item.
        /// </param>
        /// <param name="oldItem">
        /// The original item that is replaced.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If action is not Replace.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object newItem, object oldItem)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif

            if (action == NotifyCollectionChangedAction.Replace)
            {
                var objArray = new object[1];
                objArray[0] = newItem;
                var objArray1 = new object[1];
                objArray1[0] = oldItem;
                this.InitializeMoveOrReplace(action, objArray, objArray1, -1, -1);
                return;
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(string.Format(local.Get("WrongActionForCtor"), NotifyCollectionChangedAction.Replace), "action");
#else
            throw new ArgumentException(string.Format(Resources.WrongActionForCtor, NotifyCollectionChangedAction.Replace), "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a one-item <see cref="NotifyCollectionChangedAction.Replace"/> change.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This can be set to <see cref="NotifyCollectionChangedAction.Replace"/>.
        /// </param>
        /// <param name="newItem">
        /// The new item that is replacing the original item.
        /// </param>
        /// <param name="oldItem">
        /// The original item that is replaced.
        /// </param>
        /// <param name="index">
        /// he index of the item being replaced.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If action is not Replace.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object newItem, object oldItem, int index)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif
            
            if (action == NotifyCollectionChangedAction.Replace)
            {
                var objArray = new object[1];
                objArray[0] = newItem;
                var objArray1 = new object[1];
                objArray1[0] = oldItem;
                this.InitializeMoveOrReplace(action, objArray, objArray1, index, index);
                return;
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(string.Format(local.Get("WrongActionForCtor"), NotifyCollectionChangedAction.Replace), "action");
#else
            throw new ArgumentException(string.Format(Resources.WrongActionForCtor, NotifyCollectionChangedAction.Replace), "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a multi-item <see cref="NotifyCollectionChangedAction.Replace"/> change.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This can only be set to <see cref="NotifyCollectionChangedAction.Replace"/>.
        /// </param>
        /// <param name="newItems">
        /// The new items that are replacing the original items.
        /// </param>
        /// <param name="oldItems">
        /// The original items that are replaced.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If oldItems or newItems is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// If action is not Replace.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList newItems, IList oldItems)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif
            
            if (action == NotifyCollectionChangedAction.Replace)
            {
                if (newItems != null)
                {
                    if (oldItems != null)
                    {
                        this.InitializeMoveOrReplace(action, newItems, oldItems, -1, -1);
                        return;
                    }

                    throw new ArgumentNullException("oldItems");
                }

                throw new ArgumentNullException("newItems");
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(string.Format(local.Get("WrongActionForCtor"), NotifyCollectionChangedAction.Replace), "action");
#else
            throw new ArgumentException(string.Format(Resources.WrongActionForCtor, NotifyCollectionChangedAction.Replace), "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a multi-item <see cref="NotifyCollectionChangedAction.Replace"/> change.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This can only be set to <see cref="NotifyCollectionChangedAction.Replace"/>.
        /// </param>
        /// <param name="newItems">
        /// The new items that are replacing the original items.
        /// </param>
        /// <param name="oldItems">
        /// The original items that are replaced.
        /// </param>
        /// <param name="startingIndex">
        /// The index of the first item of the items that are being replaced.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If oldItems or newItems is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// If action is not Replace.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList newItems, IList oldItems, int startingIndex)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif
            
            if (action == NotifyCollectionChangedAction.Replace)
            {
                if (newItems != null)
                {
                    if (oldItems != null)
                    {
                        this.InitializeMoveOrReplace(action, newItems, oldItems, startingIndex, startingIndex);
                        return;
                    }

                    throw new ArgumentNullException("oldItems");
                }

                throw new ArgumentNullException("newItems");
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(string.Format(local.Get("WrongActionForCtor"), NotifyCollectionChangedAction.Replace), "action");
#else
            throw new ArgumentException(string.Format(Resources.WrongActionForCtor, NotifyCollectionChangedAction.Replace), "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a one-item <see cref="NotifyCollectionChangedAction.Move"/> change.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This can only be set to <see cref="NotifyCollectionChangedAction.Move"/>.
        /// </param>
        /// <param name="changedItem">
        /// The item affected by the change.
        /// </param>
        /// <param name="index">
        /// The new index for the changed item.
        /// </param>
        /// <param name="oldIndex">
        /// The old index for the changed item.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If action is not Move or index is less than 0.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem, int index, int oldIndex)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif
            
            if (action == NotifyCollectionChangedAction.Move)
            {
                if (index >= 0)
                {
                    var objArray = new object[1];
                    objArray[0] = changedItem;
                    var objArray1 = objArray;
                    this.InitializeMoveOrReplace(action, objArray1, objArray1, index, oldIndex);
                    return;
                }

#if UNITY3D
                local = LocalizationManager.Instance;
                throw new ArgumentException(local.Get("IndexCannotBeNegative"), "index");
#else
                throw new ArgumentException(Resources.IndexCannotBeNegative, "index");
#endif
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(string.Format(local.Get("WrongActionForCtor"), NotifyCollectionChangedAction.Move), "action");
#else
            throw new ArgumentException(string.Format(Resources.WrongActionForCtor, NotifyCollectionChangedAction.Move), "action");
#endif
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifyCollectionChangedEventArgs"/> class that describes a multi-item <see cref="NotifyCollectionChangedAction.Move"/> change.
        /// </summary>
        /// <param name="action">
        /// The action that caused the event. This can only be set to <see cref="NotifyCollectionChangedAction.Move"/>.
        /// </param>
        /// <param name="changedItems">
        /// The items affected by the change.
        /// </param>
        /// <param name="index">
        /// The new index for the changed items.
        /// </param>
        /// <param name="oldIndex">
        /// The old index for the changed items.
        /// </param>
        /// <exception cref="ArgumentException">
        /// If action is not Move or index is less than 0.
        /// </exception>
        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems, int index, int oldIndex)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;

#if UNITY3D
            LocalizationManager local;
#endif
            
            if (action == NotifyCollectionChangedAction.Move)
            {
                if (index >= 0)
                {
                    this.InitializeMoveOrReplace(action, changedItems, changedItems, index, oldIndex);
                    return;
                }

#if UNITY3D
                local = LocalizationManager.Instance;
                throw new ArgumentException(local.Get("IndexCannotBeNegative"), "index");
#else
                throw new ArgumentException(Resources.IndexCannotBeNegative, "index");
#endif
            }

#if UNITY3D
            local = LocalizationManager.Instance;
            throw new ArgumentException(string.Format(local.Get("WrongActionForCtor"), NotifyCollectionChangedAction.Move), "action");
#else
            throw new ArgumentException(string.Format(Resources.WrongActionForCtor, NotifyCollectionChangedAction.Move), "action");
#endif
        }

        /// <summary>
        /// Gets the action that caused the event.
        /// </summary>
        public NotifyCollectionChangedAction Action
        {
            get
            {
                return this.action;
            }
        }

        /// <summary>
        /// Gets the list of new items involved in the change.
        /// </summary>
        public IList NewItems
        {
            get
            {
                return this.newItems;
            }
        }

        /// <summary>
        /// Gets the index at which the change occurred.
        /// </summary>
        public int NewStartingIndex
        {
            get
            {
                return this.newStartingIndex;
            }
        }

        /// <summary>
        /// Gets the list of items affected by a Replace, Remove, or Move action.
        /// </summary>
        public IList OldItems
        {
            get
            {
                return this.oldItems;
            }
        }

        /// <summary>
        /// Gets the index at which a Move, Remove, or Replace action occurred.
        /// </summary>
        public int OldStartingIndex
        {
            get
            {
                return this.oldStartingIndex;
            }
        }

        /// <summary>
        /// Initializes a <see cref="NotifyCollectionChangedAction.Add"/> action.
        /// </summary>
        /// <param name="actionToPerform">The action to perform.</param>
        /// <param name="newItemsList">The new items to be added.</param>
        /// <param name="newStartIndex">The starting index where the new items will be added to.</param>
        private void InitializeAdd(NotifyCollectionChangedAction actionToPerform, IList newItemsList, int newStartIndex)
        {
            object obj;
            this.action = actionToPerform;
            var notifyCollectionChangedEventArg = this;
            if (newItemsList == null)
            {
                obj = null;
            }
            else
            {
                obj = new ReadOnlyList(newItemsList); 
            }

            notifyCollectionChangedEventArg.newItems = (IList)obj;
            this.newStartingIndex = newStartIndex;
        }

        /// <summary>
        /// Initializes a <see cref="NotifyCollectionChangedAction.Add"/> or <see cref="NotifyCollectionChangedAction.Remove"/> action.
        /// </summary>
        /// <param name="actionToPerform">The action to perform.</param>
        /// <param name="changedItems">
        /// The list of changed items.
        /// </param>
        /// <param name="startingIndex">The starting index where the new items will be added or removed from.</param>
        private void InitializeAddOrRemove(NotifyCollectionChangedAction actionToPerform, IList changedItems, int startingIndex)
        {
            if (actionToPerform != NotifyCollectionChangedAction.Add)
            {
                if (actionToPerform != NotifyCollectionChangedAction.Remove)
                {
                    return;
                }

                this.InitializeRemove(actionToPerform, changedItems, startingIndex);
                return;
            }

            this.InitializeAdd(actionToPerform, changedItems, startingIndex);
        }

        /// <summary>
        /// Initializes a <see cref="NotifyCollectionChangedAction.Move"/> or <see cref="NotifyCollectionChangedAction.Replace"/> action.
        /// </summary>
        /// <param name="actionToPerform">The action to perform.</param>
        /// <param name="newItemsList">
        /// The new items to be moved or replaced.
        /// </param>
        /// <param name="oldItemsList">
        /// The old items that were removed or replaced.
        /// </param>
        /// <param name="startingIndex">
        /// The starting index.
        /// </param>
        /// <param name="oldStartIndex">
        /// The old starting index.
        /// </param>
        private void InitializeMoveOrReplace(NotifyCollectionChangedAction actionToPerform, IList newItemsList, IList oldItemsList, int startingIndex, int oldStartIndex)
        {
            this.InitializeAdd(actionToPerform, newItemsList, startingIndex);
            this.InitializeRemove(actionToPerform, oldItemsList, oldStartIndex);
        }

        /// <summary>
        /// Initializes a <see cref="NotifyCollectionChangedAction.Add"/> action.
        /// </summary>
        /// <param name="actionToPerform">The action to perform.</param>
        /// <param name="oldItemsList">The old items to be removed.</param>
        /// <param name="oldStartIndex">The starting index where the old items were removed from.</param>
        private void InitializeRemove(NotifyCollectionChangedAction actionToPerform, IList oldItemsList, int oldStartIndex)
        {
            object obj;
            this.action = actionToPerform;
            var notifyCollectionChangedEventArg = this;
            if (oldItemsList == null)
            {
                obj = null;
            }
            else
            {
                obj = new ReadOnlyList(oldItemsList);
            }

            notifyCollectionChangedEventArg.oldItems = (IList)obj;
            this.oldStartingIndex = oldStartIndex;
        }
    }
}