﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.PropertyModifiers
{
    using System.Collections.Generic;

    /// <summary>
    /// Float based property.
    /// </summary>
    public class FloatProperty : IProperty<float>
    {
        #region Constants and Fields

        /// <summary>
        /// The modifier list.
        /// </summary>
        private readonly IList<IPropertyModifier<float>> modifiers = new List<IPropertyModifier<float>>();

        /// <summary>
        /// The base value for the property.
        /// </summary>
        private float baseValue;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatProperty"/> class.
        /// </summary>
        public FloatProperty()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FloatProperty"/> class.
        /// </summary>
        /// <param name="value">
        /// Specifies the value of the <see cref="BaseValue"/> property.
        /// </param>
        public FloatProperty(float value)
            : this()
        {
            this.baseValue = value;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the base value of the property.
        /// </summary>
        public virtual float BaseValue
        {
            get
            {
                return this.baseValue;
            }

            set
            {
                this.baseValue = value;
            }
        }

        /// <summary>
        /// Gets the number of modifier attached to the property.
        /// </summary>
        public int ModifierCount
        {
            get
            {
                return this.modifiers.Count;
            }
        }

        /// <summary>
        /// Gets a <see cref="IEnumerable{T}"/> of property modifier objects.
        /// </summary>
        public IEnumerable<IPropertyModifier<float>> Modifiers
        {
            get
            {
                return this.modifiers;
            }
        }

        /// <summary>
        /// Gets the modified value of the property.
        /// </summary>            
        public virtual float Value
        {
            get
            {
                // get the base value
                var value = this.baseValue;

                // process all modifiers
                for (var index = this.modifiers.Count - 1; index >= 0; index--)
                {
                    var modifier = this.modifiers[index];
                    switch (modifier.Type)
                    {
                        case ModifierType.Addition:
                            value += modifier.Value;
                            break;

                        case ModifierType.Subtraction:
                            value -= modifier.Value;
                            break;

                        case ModifierType.Multiply:
                            value *= modifier.Value;
                            break;

                        case ModifierType.Divide:
                            value /= modifier.Value;
                            break;

                        case ModifierType.Absolute:
                            return modifier.Value;
                    }
                }

                return value;
            }
        }

        /// <summary>
        /// Adds a modifier to the modifier list.
        /// </summary>
        /// <param name="modifier">A reference to the modifier the will be added.</param>
        public void AddModifier(IPropertyModifier<float> modifier)
        {
            if (this.modifiers.Count == 0)
            {
                this.modifiers.Add(modifier);
                return;
            }

            var added = false;
            for (var i = 0; i < this.modifiers.Count; i++)
            {
                if (modifier.Priority >= this.modifiers[i].Priority)
                {
                    continue;
                }

                this.modifiers.Insert(i, modifier);
                added = true;
                break;
            }

            if (!added)
            {
                this.modifiers.Add(modifier);
            }
        }

        /// <summary>
        /// Removes a modifier from the modifier list.
        /// </summary>
        /// <param name="modifier">A reference to the modifier the will be removed.</param>
        public void RemoveModifier(IPropertyModifier<float> modifier)
        {
            this.modifiers.Remove(modifier);
        }

        #endregion
    }
}