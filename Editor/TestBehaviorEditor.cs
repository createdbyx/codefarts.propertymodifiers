﻿namespace Codefarts.PropertyModifiers.Code.Editor
{
    using Codefarts.PropertyModifiers.Scripts;

    using UnityEditor;

    using UnityEngine;

    [CustomEditor(typeof(TestBehaviour))]
    public class TestBehaviorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            //  base.DrawDefaultInspector();

            var props = this.target as TestBehaviour;
            if (props == null)
            {
                GUILayout.Label("Bad target", "ErrorLabel");
                return;
            }

            if (props.Properties != null)
            {
                foreach (var pair in props.Properties)
                {
                    pair.Value.BaseValue = EditorGUILayout.FloatField(pair.Key + ": " + pair.Value, pair.Value.BaseValue);
                }
            }

            if (GUILayout.Button("Add") && props.Properties != null)
            {
                props.Properties.Add("Speed", new FloatProperty(5));
            }
        }
    }
}