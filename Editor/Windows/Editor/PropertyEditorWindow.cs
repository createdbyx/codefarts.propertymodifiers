﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.PropertyModifiers.Editor
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Codefarts.PropertyModifiers.Editor.Controls;
    using Codefarts.PropertyModifiers.Editor.Models;
    using Codefarts.PropertyModifiers.Scripts.Character;
    using Codefarts.PropertyModifiers.Unity;

    using UnityEditor;

    using UnityEngine;

    /// <summary>
    /// Provides an editor window for editing property files.
    /// </summary>
    public class PropertyEditorWindow : EditorWindow
    {
        /// <summary>
        /// Holds a reference to a table control.
        /// </summary>
        private readonly Table<PropertyModel> entriesTable;

        /// <summary>
        /// Holds a reference to a table control.
        /// </summary>
        private readonly Table<ModifierModel> modifierTable;

        /// <summary>
        /// Holds the filter model.
        /// </summary>
        private readonly ModifierTableModel modifierTableModel;

        /// <summary>
        /// Holds the asset sync model. 
        /// </summary>
        private readonly PropertyTableModel tableModel;

        /// <summary>
        /// Holds the current file name for the entries.
        /// </summary>
        private string fileName;

        /// <summary>
        /// Records if entries have changed.
        /// </summary>
        private bool isDirty;

        /// <summary>
        /// Used to determine if unity has recompiled scripts.
        /// </summary>
        private RecompileClass recompile;

        /// <summary>
        /// Holds the scroll value for the table.
        /// </summary>
        private Vector2 scrollPosition;

        /// <summary>
        /// Holds a list of modifier types.
        /// </summary>
        private Type[] availableModifierTypes;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyEditorWindow"/> class.
        /// </summary>
        public PropertyEditorWindow()
        {
            this.tableModel = new PropertyTableModel();
            this.modifierTableModel = new ModifierTableModel();
            this.entriesTable = new Table<PropertyModel> { AlwaysDraw = true, Model = this.tableModel, RowHeight = 18 };
            this.modifierTable = new Table<ModifierModel> { AlwaysDraw = true, Model = this.modifierTableModel, RowHeight = 18, TableName = "Modifiers" };
            this.modifierTable.Model = this.modifierTableModel;
        }

        /// <summary>
        /// Shows the <see cref="PropertyEditorWindow"/> window.
        /// </summary>
        [MenuItem("Window/Codefarts/Property Editor Window")]
        public static void ShowWindow()
        {
            var window = GetWindow<PropertyEditorWindow>();
            window.autoRepaintOnSceneChange = true;
            window.title = "Property Editor";
            window.Show();
        }

        /// <summary>
        /// Return true if one or more of the entries has changed.
        /// </summary>
        /// <returns>true if there have been changes; otherwise false.</returns>
        public bool HasChanges()
        {
            return this.isDirty || this.tableModel.Elements.Any(model => model.IsDirty) ||
               (this.modifierTableModel.Elements != null && this.modifierTableModel.Elements.Any(model => model.IsDirty));
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            this.GetAvailableModifiers(true);
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            // check if recompile variable is null and a filename has been set and still exists 
            if (this.recompile == null && !string.IsNullOrEmpty(this.fileName) && File.Exists(this.fileName))
            {
                // if yes then reload data and create a reference to a recompile class 
                this.LoadData(false);
                this.recompile = new RecompileClass();
            }

            GUILayout.BeginVertical(GUILayout.ExpandWidth(true));

            // draw the button along the top of the window
            this.DrawHeaderButtons();

            if (this.modifierTableModel.Elements != null)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Done", GUILayout.MaxHeight(25), GUILayout.Height(25)))
                {
                    this.modifierTableModel.Elements = null;
                }

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }

            // draw the table
            this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition, false, false);
            if (this.modifierTableModel.Elements != null)
            {
                this.modifierTable.Draw();
            }
            else
            {
                this.entriesTable.Draw();
            }

            GUILayout.EndScrollView();

            GUILayout.EndVertical();
        }

        /// <summary>
        /// Handles the click event for the add button.
        /// </summary>
        private void AddClick()
        {
            if (this.modifierTableModel.Elements != null && this.availableModifierTypes.Length > 0)
            {
                this.modifierTableModel.Elements.Add(new ModifierModel()
                    {
                        TypeCallback = this.OnTypeCallback,
                        RemoveCallback = this.OnRemoveModifierCallback
                    });
                return;
            }

            // add a new model to the table
            var model = new PropertyModel() { Name = string.Empty };
            model.RemoveCallback += this.OnPropertyRemoveCallback;
            model.ModifiersCallback += this.OnModifierCallback;
            this.tableModel.Elements.Add(model);
            this.isDirty = true;
        }

        /// <summary>
        /// Draws action buttons at the top of the window.
        /// </summary>
        private void DrawHeaderButtons()
        {
            // draw the path and filename along the top
            GUILayout.BeginHorizontal();
            GUILayout.Label("File Name: ");
            GUILayout.TextField(this.fileName ?? string.Empty);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            // draw the buttons
            GUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();
            if (GUILayout.Button("New", GUILayout.MinHeight(32)))
            {
                this.StartNewList();
            }

            if (GUILayout.Button("Load", GUILayout.MinHeight(32)))
            {
                this.LoadData(true);
            }

            if (this.HasChanges() && GUILayout.Button("Save", GUILayout.MinHeight(32)))
            {
                this.SaveData(false);
            }

            if (GUILayout.Button("Save As", GUILayout.MinHeight(32)))
            {
                this.SaveData(true);
            }

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Add", GUILayout.MinWidth(60), GUILayout.MinHeight(32)))
            {
                this.AddClick();
            }

            GUILayout.EndHorizontal();
        }

        /// <summary>
        /// Loads sync entries from a xml file.
        /// </summary>
        /// <param name="showDialog">
        /// true to show the open file dialog; otherwise false.
        /// </param>
        private void LoadData(bool showDialog)
        {
            var file = showDialog ? string.Empty : this.fileName;
            PropertyModel[] syncModels;
            if (this.TryToLoadSyncData(showDialog, file, out syncModels))
            {
                return;
            }

            // start a new sync list
            this.StartNewList();

            // add models to the table model list
            foreach (var model in syncModels)
            {
                this.tableModel.Elements.Add(model);
            }

            // enforce directory separation characters (no alt chars)
            file = file.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

            // set the file name 
            this.fileName = file;
        }

        /// <summary>
        /// Handles the callback for displaying a modifiers button.
        /// </summary>
        /// <param name="row">The cell row of the model in question.</param>
        /// <param name="column">The cell column of the model in question.</param>
        /// <param name="model">Reference to the model responsible for accessing table cells.</param>
        private void OnModifierCallback(int row, int column, PropertyModel model)
        {
            var propertyTableModel = ((PropertyTableModel)this.entriesTable.Model).Elements[row];

            // provide a button to set the filtering info
            var text = propertyTableModel.Modifiers != null ? propertyTableModel.Modifiers.Count.ToString(CultureInfo.InvariantCulture) : "0";
            if (GUILayout.Button(text, GUILayout.MaxWidth(55), GUILayout.MaxHeight(this.entriesTable.RowHeight - 1)))
            {
                this.modifierTableModel.Elements = propertyTableModel.Modifiers;
            }
        }

        /// <summary>
        /// Handles displaying a remove button for removing entries.
        /// </summary>
        /// <param name="row">The cell row of the model in question.</param>
        /// <param name="column">The cell column of the model in question.</param>
        /// <param name="model">Reference to the model responsible for accessing table cells.</param>
        private void OnPropertyRemoveCallback(int row, int column, PropertyModel model)
        {
            // if button clicked remove button at row
            if (GUILayout.Button("Remove", GUILayout.MaxWidth(60), GUILayout.MaxHeight(this.entriesTable.RowHeight - 1)))
            {
                var m = (PropertyTableModel)this.entriesTable.Model;
                m.Elements.RemoveAt(row);
                this.isDirty = true;
            }
        }

        /// <summary>
        /// Handles displaying a remove button for removing entries.
        /// </summary>
        /// <param name="row">The cell row of the model in question.</param>
        /// <param name="column">The cell column of the model in question.</param>
        /// <param name="model">Reference to the model responsible for accessing table cells.</param>
        private void OnRemoveModifierCallback(int row, int column, PropertyModel model)
        {
            // if button clicked remove button at row
            if (GUILayout.Button("Remove", GUILayout.MaxWidth(60), GUILayout.MaxHeight(this.modifierTable.RowHeight)))
            {
                var m = (ModifierTableModel)this.modifierTable.Model;
                m.Elements.RemoveAt(row);
                this.isDirty = true;
            }
        }

        /// <summary>
        /// Searches through assemblies and finds types that implement the <see cref="IPropertyModifier{T}"/> interface.
        /// </summary>
        /// <param name="force">true is you want to force a rebuild of the available tools list.</param>
        private void GetAvailableModifiers(bool force)
        {
            // if not forcing and there are already available tools just exit
            if (!force && this.availableModifierTypes != null)
            {
                return;
            }

            // search for types in each assembly that implement the IEditorTool interface
            var list = new List<Type>();
            var fullName = typeof(IPropertyModifier<float>).Name;
            foreach (var type in AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes()))
            {
                try
                {
                    var interfaceType = type.GetInterfaces().FirstOrDefault(x => x.Name == fullName);
                    if (interfaceType != null)
                    {
                        list.Add(type);
                    }
                }
                catch
                {
                    // ignore error
                }
            }

            // set the available tools list
            this.availableModifierTypes = list.OrderBy(x => x.FullName).ToArray();
        }

        /// <summary>
        /// Handles the callback for the modifier type.
        /// </summary>
        /// <param name="row">The cell row of the model in question.</param>
        /// <param name="column">The cell column of the model in question.</param>
        /// <param name="model">Reference to the model responsible for accessing table cells.</param>
        private void OnTypeCallback(int row, int column, ModifierModel model)
        {
            if (this.availableModifierTypes.Length == 0)
            {
                return;
            }

            var availableModifierNames = this.availableModifierTypes.Select(
                x =>
                {
                    var typeName = x.FullName;
                    var searchIndex = typeName.IndexOf("`", StringComparison.Ordinal);
                    var count = 0;
                    if (searchIndex != -1)
                    {
                        typeName = typeName.Insert(searchIndex, "<");
                    }

                    for (var i = 0; i < 100; i++)
                    {
                        searchIndex = typeName.IndexOf("`", StringComparison.Ordinal);
                        if (searchIndex == -1)
                        {
                            break;
                        }

                        count += searchIndex != -1 ? 1 : 0;
                        typeName = typeName.Replace("`" + i, string.Empty);
                    }

                    typeName += new string(',', count < 0 ? 0 : count) + (count > 0 ? ">" : string.Empty);
                    return typeName;
                }).ToArray();

            var currentIndex = Array.IndexOf(this.availableModifierTypes, model.Type);
            if (currentIndex < 0)
            {
                currentIndex = 0;
            }

            var index = EditorGUILayout.Popup(currentIndex, availableModifierNames);
            if (index != -1)
            {
                model.Type = this.availableModifierTypes[index];
            }
        }

        /// <summary>
        /// Save the current entries to disk.
        /// </summary>
        /// <param name="saveNew">
        /// If true will display the save file dialog.
        /// </param>
        private void SaveData(bool saveNew)
        {
            // if filename not set or saveNew is true then prompt user to specify a save location
            if (string.IsNullOrEmpty(this.fileName) || saveNew)
            {
                // prompt user
                var file = EditorUtility.SaveFilePanel("Save property data", string.Empty, "Properties.xml", "xml");
                if (file.Length == 0)
                {
                    // user canceled so just exit
                    return;
                }

                // enforce directory separation characters (no alt chars)
                file = file.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

                // save file name
                this.fileName = file;
            }

            // generate xml markup from the model data
            var data = "<?xml version=\"1.0\"?>\r\n<properties>\r\n{0}\r\n</properties>";
            var entries = string.Join(
                "\r\n",
                (from item in this.tableModel.Elements
                 let filters = string.Join(
                     "\r\n",
                     item.Modifiers.Select(f => string.Format("            <modifier type=\"{0}\" />\r\n", f.Type.FullName)).ToArray())
                 select string.Format(
                     "    <property name=\"{0}\" value=\"{1}\"" + (filters.Length == 0 ? " /" : string.Empty) + ">\r\n" +
                     "    {2}" +
                     (filters.Length == 0 ? string.Empty : "    </property>"),
                     item.Name,
                     item.Value.BaseValue,
                     filters)).ToArray());

            data = string.Format(data, entries);

            // save data to a xml file
            File.WriteAllText(this.fileName, data);

            // clear the isdirty flags for each model 
            foreach (var model in this.tableModel.Elements)
            {
                model.IsDirty = false;
            }

            this.isDirty = false;

            // refresh the asset data base to ensure any changes are picked up
            AssetDatabase.Refresh();
        }

        /// <summary>
        /// Starts a new property list.
        /// </summary>
        private void StartNewList()
        {
            this.tableModel.Elements.Clear();
            if (this.modifierTableModel.Elements != null)
            {
                this.modifierTableModel.Elements.Clear();
                this.modifierTableModel.Elements = null;
            }

            this.fileName = null;
            this.isDirty = false;
        }

        /// <summary>
        /// Attempts to load the property data from a xml file.
        /// </summary>
        /// <param name="showDialog">If true will show a open file dialog so the user can select a xml file.</param>
        /// <param name="file">The file that the user selected to load.</param>
        /// <param name="models">The data that was loaded.</param>
        /// <returns>Returns True if successful.</returns>
        private bool TryToLoadSyncData(bool showDialog, string file, out PropertyModel[] models)
        {
            models = null;
            file = showDialog ? null : file;
            if (showDialog)
            {
                // prompt user to select a sync file
                file = EditorUtility.OpenFilePanel("Load property data", string.Empty, "xml");
                if (file.Length == 0)
                {
                    return true;
                }
            }

            // enforce directory separation characters (no alt chars)
            file = file.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

            // reader the property data
            var reader = new PropertiesReader();
            var props = reader.Read(file, null) as CharacterProperties;

            if (props != null)
            {
                // store the data in the table model
                var results = props.Select(x => new PropertyModel() { Name = x.Key, Value = x.Value });
                models = results.ToArray();
                this.tableModel.Elements = results.ToList();
            }

            return true;
        }

        /// <summary>
        /// Private class used to determine if unity has recompiled scripts.
        /// </summary>
        private class RecompileClass
        {
        }
    }
}