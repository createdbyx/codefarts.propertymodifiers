﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.PropertyModifiers.Editor.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a table model for properties that implements <see cref="ITableModel{T}"/>.
    /// </summary>
    public class PropertyTableModel : ITableModel<PropertyModel>
    {
        /// <summary>
        /// Holds the value for the <see cref="Elements"/> property.
        /// </summary>
        private IList<PropertyModel> elements = new List<PropertyModel>();

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyTableModel"/> class.
        /// </summary>
        /// <param name="elements">
        /// The elements.
        /// </param>
        public PropertyTableModel(IList<PropertyModel> elements)
        {
            this.elements = elements;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyTableModel"/> class.
        /// </summary>
        public PropertyTableModel()
        {
        }

        /// <summary>
        /// Gets the column count.
        /// </summary>
        public int ColumnCount
        {
            get
            {
                return 5;
            }
        }

        /// <summary>
        /// Gets or sets the elements.
        /// </summary>
        public IList<PropertyModel> Elements
        {
            get
            {
                return this.elements;
            }

            set
            {
                this.elements = value;
            }
        }

        /// <summary>
        /// Gets the row count.
        /// </summary>
        public int RowCount
        {
            get
            {
                return this.elements == null ? 0 : this.elements.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not headers shown be shown.
        /// </summary>
        public bool UseHeaders
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets whether or not a cell can be edited.
        /// </summary>
        /// <param name="rowIndex">The index of the row to retrieve the data from.</param>
        /// <param name="columnIndex">The index of the column to retrieve the data from.</param>
        /// <returns>true if the cell can be edited; otherwise false.</returns>
        public bool CanEdit(int rowIndex, int columnIndex)
        {
            switch (columnIndex)
            {
                case 0:
                case 1:
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the name of a column.
        /// </summary>
        /// <param name="columnIndex">The index of the column to retrieve.</param>
        /// <returns>Returns the column name.</returns>
        public string GetColumnName(int columnIndex)
        {
            switch (columnIndex)
            {
                case 0:
                    return "Name";

                case 1:
                    return "Base value";

                case 2:
                    return "Value";

                case 3:
                    return "Modifiers";   
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the column width.
        /// </summary>
        /// <param name="columnIndex">The index of the column to retrieve.</param>
        /// <returns>Returns the width of the column.</returns>
        public int GetColumnWidth(int columnIndex)
        {
            switch (columnIndex)
            {
                  case 3:
                    return 55;
                case 4:
                    return 60;
            }

            return 0;
        }

        /// <summary>
        /// Gets the value for a table cell.
        /// </summary>
        /// <param name="rowIndex">The index of the row to retrieve the data from.</param>
        /// <param name="columnIndex">The index of the column to retrieve the data from.</param>
        /// <returns>Returns the value of the cell.</returns>
        public object GetValue(int rowIndex, int columnIndex)
        {
            if (rowIndex < 0 || rowIndex >= this.elements.Count)
            {
                return null;
            }

            var el = this.elements[rowIndex];
            switch (columnIndex)
            {
                case 0:
                    return el.Name;

                case 1:
                    return el.Value.BaseValue;

                case 2:
                    return el.Value.Value;

                case 3:
                    return el.ModifiersCallback;
            
                case 4:
                    return el.RemoveCallback;
            }

            return "Unknown";
        }

        /// <summary>
        /// Sets the value for a table cell.
        /// </summary>
        /// <param name="rowIndex">The index of the row to set the data.</param>
        /// <param name="columnIndex">The index of the column to set the data.</param>
        /// <param name="value">The value to assign to the cell.</param>
        public void SetValue(int rowIndex, int columnIndex, object value)
        {
            if (rowIndex < 0 || rowIndex >= this.elements.Count)
            {
                return;
            }

            var el = this.elements[rowIndex];
            switch (columnIndex)
            {
                case 0:
                    el.Name = (string)value;
                    break;

                case 1:
                    el.Value.BaseValue = (float)value;
                    break;
            }
        }
    }
}
