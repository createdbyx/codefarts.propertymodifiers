﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.PropertyModifiers.Editor.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Provides a class for properties.
    /// </summary>
    public class PropertyModel
    {
        /// <summary>
        /// Holds the value of the <see cref="Modifiers"/> property.
        /// </summary>
        private readonly IList<ModifierModel> modifiers = new List<ModifierModel>();

        /// <summary>
        /// Holds the value of the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyModel"/> class.
        /// </summary>
        public PropertyModel()
        {
            this.Value = new FloatProperty();
        }

        /// <summary>
        /// Gets or sets a value indicating whether the property has changed.
        /// </summary>
        public bool IsDirty { get; set; }

        /// <summary>
        /// Gets a list of <see cref="ModifierModel"/> types.
        /// </summary>
        public IList<ModifierModel> Modifiers
        {
            get
            {
                return this.modifiers;
            }    
        }

        /// <summary>
        /// Gets or sets a callback that will be used to draw the modifier count.
        /// </summary>
        public Action<int, int, PropertyModel> ModifiersCallback { get; set; }

        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name == value)
                {
                    return;
                }

                this.name = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a callback that will be used to draw a remove button.
        /// </summary>
        public Action<int, int, PropertyModel> RemoveCallback { get; set; }

        /// <summary>
        /// Gets or sets the property value. 
        /// </summary>
        public FloatProperty Value { get; set; }
    }
}