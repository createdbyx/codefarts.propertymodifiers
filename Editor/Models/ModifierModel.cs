﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.PropertyModifiers.Editor.Models
{
    using System;

    /// <summary>
    /// Provides a modifier model for a <see cref="ModifierTableModel"/> implementation.
    /// </summary>
    public class ModifierModel
    {
        /// <summary>
        /// Holds the value for the <see cref="Type"/> property.
        /// </summary>
        private Type type;

        /// <summary>
        /// Gets or sets a value indicating whether the property has changed.
        /// </summary>
        public bool IsDirty { get; set; }

        /// <summary>
        /// Gets or sets the modifier type. 
        /// </summary>
        public Type Type
        {
            get
            {
                return this.type;
            }
         
            set
            {
                if (this.type == value)
                {
                    return;
                }

                this.type = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a callback that will be used to draw a popup list of modifiers.
        /// </summary>
        public Action<int, int, ModifierModel> TypeCallback { get; set; }

        /// <summary>
        /// Gets or sets a callback that will be used to draw a remove button.
        /// </summary>
        public Action<int, int, PropertyModel> RemoveCallback { get; set; }
    }
}
