﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.PropertyModifiers
{
    using System.Collections.Generic;

    /// <summary>
    /// The IProperty interface.
    /// </summary>
    /// <typeparam name="T">
    /// The type used as the property value.
    /// </typeparam>
    public interface IProperty<T>
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the base value for the property.
        /// </summary>
        T BaseValue { get; set; }

        /// <summary>
        /// Gets the number of modifier attached to the property.
        /// </summary>
        int ModifierCount { get; }

        /// <summary>
        /// Gets a <see cref="IEnumerable{T}"/> of property modifier objects.
        /// </summary>
        IEnumerable<IPropertyModifier<T>> Modifiers { get; }

        /// <summary>
        /// Gets the modified value of the property.
        /// </summary>
        T Value { get; }

        /// <summary>
        /// Adds a modifier to the modifier list.
        /// </summary>
        /// <param name="modifier">A reference to the modifier the will be added.</param>
        void AddModifier(IPropertyModifier<T> modifier);

        /// <summary>
        /// Removes a modifier from the modifier list.
        /// </summary>
        /// <param name="modifier">A reference to the modifier the will be removed.</param>
        void RemoveModifier(IPropertyModifier<T> modifier);

        #endregion
    }
}